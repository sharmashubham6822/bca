import csv

header=["student name","marks"]
student_list=[["cherry","50"],["sahil","60"],["shubham","70"]]

with open("student_list",'w') as stud:
    student=csv.writer(stud)
    student.writerow(header)
    student.writerow(student_list)
