#include <stdio.h>
#include <conio.h>
void main()
{
    int x, y, temp;
    printf("Enter the value of X and Y");
    scanf("%d%d", &x, &y);
    printf("Before swapping\nx=%d", x, y);
    temp = x;
    x = y;
    y = temp;
    printf("After swapping\nx=%d", x, y);
}